import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input,Dense
from tensorflow.keras.activations import sigmoid
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import SGD
import numpy as np
import sys

def one_step(current_state,current_energy,n_feat,cwidth,temp=1):
  feat=tf.random.uniform((),0,n_feat,dtype=tf.int32)
  new_val=tf.random.uniform((current_state.shape[0],),cwidth[feat],cwidth[feat+1],dtype=tf.int32)
  new_state=current_state
  new_state[:,feat]=new_val
  x=tf.reduce_sum(tf.one_hot(new_state,cwidth[-1],axis=-1,dtype=tf.float16),axis=1)
  new_energy=energy.predict(x)
  DeltaE=(new_energy-current_energy)
  temp=np.mean(DeltaE[DeltaE<0])/2.
  prob=tf.random.uniform(shape=DeltaE.shape)
  acc=DeltaE>0#prob<tf.math.exp(DeltaE/temp)
  acc=np.squeeze(acc)
  current_state[acc,feat]=new_val[acc]
  current_energy[acc]=new_energy[acc]
  x0=np.array(tf.reduce_sum(tf.one_hot(current_state,cwidth[-1],axis=-1,dtype=tf.float16),axis=1))
  x0[np.where(acc)]=x.numpy()[np.where(acc)]
  return current_state,tf.convert_to_tensor(x0),current_energy

# Load data
data=np.load(sys.argv[1])
print(data.shape)

# Convert row to indexes by adding cumulative width
width=np.max(data,axis=0).tolist()
cwidth=[0]
for i in range(len(width)):
  cwidth.append(cwidth[i]+width[i])
data+=np.array(cwidth[:-1])[np.newaxis,:]
data_size=cwidth[-1]
n_feat=len(width)

# generate fake data by shuffling real data
fake_data=np.copy(data)
for col in range(data.shape[1]):
  np.random.shuffle(fake_data[:,col])
fake_data=fake_data[:3000]

y=[1 for i in range(data.shape[0])]
y.extend([0 for i in range(fake_data.shape[0])])
y=np.array(y)
sw=[1./data.shape[0] for i in range(data.shape[0])]
sw.extend([1./fake_data.shape[0] for i in range(fake_data.shape[0])])
sw=data.shape[0]*np.array(sw)

x_l=Input((data_size,),name='input')
hidden_l=Dense(32,activation='softplus')(x_l)
energy_l=Dense(1,kernel_regularizer=l2(0.01),bias_regularizer=l2(0.01),name='energy')(hidden_l)
out_l=sigmoid(energy_l)

discriminator=Model(x_l,out_l,name='discriminator')
optimizer=SGD(learning_rate=0.01)
discriminator.compile(optimizer, loss='binary_crossentropy',metrics=[tf.keras.metrics.BinaryAccuracy()])

data_cat=np.vstack([tf.reduce_sum(tf.one_hot(data[i:i+100],cwidth[-1],axis=-1,dtype=tf.float16),axis=1) for i in range(0,data.shape[0],100)])
fake_data_cat=tf.reduce_sum(tf.one_hot(fake_data,cwidth[-1],axis=-1,dtype=tf.float16),axis=1)
x=np.vstack([data_cat,fake_data_cat])
discriminator.fit(x,y,sample_weight=sw,epochs=2)
energy=Model( discriminator.get_layer('input').output,discriminator.get_layer('energy').output )
real_en=energy.predict(data_cat)
fake_en=energy.predict(fake_data_cat)
while True:
  temp=1.
  re=np.mean(real_en)
  fe=np.mean(fake_en)
  print("Energy Real Before: {}".format(re))
  print("Energy Fake Before: {}".format(fe))
  while( ((re-fe)/re)>0.01 ):
    for lf in range(len(width)):
      fake_data,fake_data_cat,fake_en=one_step(fake_data,fake_en,n_feat,cwidth,temp=temp)
    fe=np.mean(fake_en)
    print(fe,re)
  x=np.vstack([data_cat,fake_data_cat])
  print("Energy Real After: {}".format(np.mean(energy.predict(data_cat))))
  print("Energy Fake After: {}".format(np.mean(energy.predict(fake_data_cat))))

  discriminator.fit(x,y,sample_weight=sw,epochs=2)  
  energy=Model( discriminator.get_layer('input').output,discriminator.get_layer('energy').output )
  real_en=energy.predict(data_cat)
  fake_en=energy.predict(fake_data_cat)
