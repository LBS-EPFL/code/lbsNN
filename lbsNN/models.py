import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Activation,Dense,Dropout,Input,Layer
from tensorflow.keras.models import load_model,Model
from tensorflow.keras.optimizers import SGD,Adam,Adadelta,RMSprop
from tensorflow.keras.callbacks import EarlyStopping,CSVLogger,ModelCheckpoint,Callback
from tensorflow.keras.regularizers import Regularizer
import tensorflow.keras.backend as K
from tensorflow.keras.utils import get_custom_objects

def get_submodel( modelfile, inlayer, outlayer ):
  model=load_model(modelfile,compile=False)
  
  op=None
  if ':' in outlayer:
    outlayer,op=outlayer.split(':')
  
  inputs=model.get_layer(inlayer).output
  output=model.get_layer(outlayer).output
  print(output)
  if op is not None:
    if op == "log":
      output=tf.keras.layers.Lambda( lambda x: tf.math.log(x) )(output)
  return Model(inputs,output,name=model.name)

def _extend_list(final_size,data,data_name):
  if len(data)==1:
    data=[data[0] for i in range(final_size)]
  if len(data)!=final_size:
    raise RuntimeError("Expected 1 or {} values for {}. Found {}".format(final_size,data_name,len(data)))
  return data

def set_random_seed(seed):
  np.random.seed(seed)
  tf.random.set_seed(seed)

def register_custom_objects():
  def rename(newname):
    def decorator(f):
        f.__name__ = newname
        return f
    return decorator
  
  @rename('pow2')
  def pow2Activation(x):
    return K.pow(x,2)
  
  @rename('pow2sign')
  def pow2signActivation(x):
    return K.pow(x,2)*K.sign(x)

  get_custom_objects().update({'pow2': pow2Activation})
  get_custom_objects().update({'pow2sign': pow2signActivation})
#  get_custom_objects().update({'lbsDropoutLayer': lbsDropout})
  get_custom_objects().update({'secondOrderLayer': secondOrderLayer})

class l1_l2_fluxes(Regularizer):
  """Regularizer for L1, L2 for Flux regularization.
  # Arguments
      l1: Float; L1 regularization factor.
      l2: Float; L2 regularization factor.
  """

  def __init__(self, l1=0., l2=0.,lj=0.):
    self.l1 = K.cast_to_floatx(l1)
    self.l2 = K.cast_to_floatx(l2)
    self.lj = K.cast_to_floatx(lj)

  def __call__(self, x):
    regularization = 0.
    if self.l1:
      regularization += self.l1 * K.sum(K.abs(x))
    if self.l2:
      regularization += self.l2 * K.sum(K.square(x))
    if self.lj:
      y=tf.linalg.set_diag(x,tf.zeros(tf.linalg.diag_part(x).shape))
      new_diag=tf.reduce_sum(y,axis=1)
      y=tf.linalg.set_diag(y,-new_diag)
      P=tf.linalg.expm(y)
      R=tf.multiply(y,P)
      R=R-tf.transpose(R,(1,0))
      regularization += self.lj * K.sum(K.square(R))
    return regularization

  def get_config(self):
    return {'l1': float(self.l1),
            'l2': float(self.l2),
            'lj': float(self.lj)}

class ensambleScheduler(Callback):
  def __init__(self,initial_rate,epochs,snapshots,filename,decay=0.,verbose=False):
    super(ensambleScheduler,self).__init__()
    self.alpha = initial_rate/2.
    self.factor = np.floor(epochs/snapshots)
    self.filename=filename
    self.snapshot_id=0
    self.verbose=verbose
    self.decay=decay
  
  def schedule(self,epoch):
    return self.alpha/(1.+self.decay*epoch)*(np.cos(np.pi*np.mod(epoch,self.factor)/self.factor) + 1)

  def on_epoch_begin(self, epoch, logs=None):
    if not hasattr(self.model.optimizer, 'lr'):
      raise ValueError('Optimizer must have a "lr" attribute.')
    lr = self.schedule(epoch)
    K.set_value(self.model.optimizer.lr, lr)
    if self.verbose > 0:
      print('\nEpoch %05d: EnsambleScheduler setting learning rate to %s.' % (epoch + 1, lr))

  def on_epoch_end(self, epoch, logs=None):
    logs['lr'] = K.get_value(self.model.optimizer.lr)
    if np.mod(epoch,self.factor)==(self.factor-1):
      f=self.filename+'_{}.h5'.format(self.snapshot_id)
      self.snapshot_id+=1
      self.model.save(f)

class secondOrderLayer(Layer):
  def __init__(self, output_dim, kernel_regularizer=None, **kwargs):
    self.output_dim = output_dim
    self.kernel_regularizer = kernel_regularizer
    super(secondOrderLayer, self).__init__(**kwargs)

  def build(self, input_shape):
    # Create a trainable weight variable for this layer.
    self.N=input_shape[0][0]
    self.kernel = self.add_weight(name='kernel', 
                                  shape=(input_shape[0][1], input_shape[0][1]),
                                  initializer='zero',
                                  regularizer=self.kernel_regularizer,
                                  trainable=True)
    super(secondOrderLayer, self).build( input_shape )  # Be sure to call this at the end

  def get_config(self):
    config = super(secondOrderLayer, self).get_config()
    config.update( {'kernel_regularizer':self.kernel_regularizer,'output_dim':self.output_dim} )
    return config

  def call(self, inputs):
    x1=inputs[0]
    x2=inputs[1]
    s=K.dot(x2, self.kernel)
    return K.sum(x1*s,axis=1,keepdims=True)
    
  def compute_output_shape(self, input_shape):
    return (input_shape[0], self.output_dim)

def build_second_order_network(inshape,kernel_regularizer,dropout=0.,seed=None,weights_filename=None):
  input_l = Input(shape=(inshape,),name='input')
  hidden1=Dropout(rate=dropout,seed=seed,name='dropout1')(input_l)
  hidden2=Dropout(rate=dropout,seed=seed+1,name='dropout2')(input_l)
  energy=secondOrderLayer(1,kernel_regularizer=kernel_regularizer[0],name='energy')([hidden1,hidden2])
  output=Activation('sigmoid',name='output')(energy)
  model=Model(input_l,output,name="second_order_network")
  if weights_filename is not None:
    model.load_weights(weights_filename, by_name=True)
  return model

def build_sequential_classifier(inshape,outshape,n_layers,units,activations,kernel_regularizers,bias_regularizers,use_bias,dropout=0.,seed=None,weights_filename=None):
  units=_extend_list(n_layers,units,'units')
  activations=_extend_list(n_layers,activations,'activations')
  kernel_regularizers=_extend_list(n_layers+1,kernel_regularizers,'kernel_regularizers')
  bias_regularizers=_extend_list(n_layers+1,bias_regularizers,'bias_regularizers')
  use_bias=_extend_list(n_layers+1,use_bias,'use_bias')

  input_l = Input(shape=(inshape,),name='input')
  hidden=Dropout(rate=dropout,seed=seed,name='dropout')(input_l)
  
  for idx in range(n_layers):
    hidden=Dense( units[idx], activation=activations[idx], kernel_regularizer=kernel_regularizers[idx], \
                  bias_regularizer=bias_regularizers[idx], use_bias=use_bias[idx], \
                  kernel_initializer='glorot_normal',bias_initializer='glorot_normal',name="hidden.{}".format(idx+1))(hidden)
  
  energy=Dense( outshape, activation=None, kernel_regularizer=kernel_regularizers[n_layers],bias_regularizer=bias_regularizers[n_layers], \
                use_bias=use_bias[n_layers],kernel_initializer='glorot_normal',bias_initializer='glorot_normal', name="energy".format(n_layers+1))(hidden)
  
  output=Activation('softmax',name='output')(energy)  
  model = Model(input_l,output,name="sequential_classifier")
  if weights_filename is not None:
    model.load_weights(weights_filename, by_name=True)
  return model

def train(model, train_data, epochs, callbacks, steps_per_epoch, val_data=(None,None,None),verbose=True):
  return model.fit(train_data,epochs=epochs,callbacks=callbacks,validation_data=val_data,steps_per_epoch=steps_per_epoch,verbose=verbose)

def predict(model,x,batch_size=None,steps=None):
  return model.predict(x,batch_size=batch_size,steps=steps)

def evaluate(model,x,y,batch_size=None,steps=None,sample_weights=None):
  return model.evaluate(x,y,batch_size=batch_size,steps=steps,sample_weight=sample_weights)

def get_optimizer(name,lr,ld,lm,ln,lh,le):
  if name=="SGD":
    return SGD(lr=lr, momentum=lm, decay=ld, nesterov=ln)
  if name=="Adam":
    return Adam(lr=lr, epsilon=le, decay=ld)
  if name=="Adadelta":
    return Adadelta(lr=lr, rho=lh, epsilon=le, decay=ld)
  if name=="RMSprop":
    return RMSprop(lr=lr, rho=lh, epsilon=le, decay=ld)

def get_callbacks(earlystopping,checkpoint,log,ensablesched):
  callbacks=[]
  if None not in earlystopping.values():
    callbacks.append( EarlyStopping(monitor=earlystopping['monitor'], patience=earlystopping['patience']) )
  if None not in checkpoint.values():
    callbacks.append( ModelCheckpoint(filepath=checkpoint['filename']+'.hdf5', verbose=0, save_best_only=True,save_weights_only=True,monitor=checkpoint['monitor']) )
  if None not in ensablesched.values():
    callbacks.append( ensambleScheduler(initial_rate=ensablesched['rate'],epochs=ensablesched['epochs'],\
                                        snapshots=ensablesched['snapshots'],filename=ensablesched['filename'],\
                                        decay=ensablesched['decay'],verbose=ensablesched['verbose']))
  if None not in log.values(): ## this must be the last callback to be added to the list!!
    callbacks.append( CSVLogger(log['filename']+'.csv'))
  return callbacks
