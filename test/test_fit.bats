#!/usr/bin/env bats

@test "Syntax: Fit zero layers noreg" {
  rm -r -f output/seq10_test0
  run lbsnn-fit -x data/seq10.npy -y output/seq10.npy -ep 10 --learning-rate 0.1 --outfolder output/seq10_test0
  [ "$status" -eq 0 ]
}

@test "Syntax: Fit zero layers noreg rerun" {
  rm -r -f output/seq10_test1
  run lbsnn-fit --from-json output/seq10_test0/args.0.json --outfolder output/seq10_test1
  [ "$status" -eq 0 ]
}

@test "Syntax: Fit zero layers reg l1 l2" {
  rm -r -f output/seq10_test2
  run lbsnn-fit -x data/seq10.npy -y output/seq10.npy -ep 10 --learning-rate 0.1 -kr 'l1(0.1)' -br 'l2(0.01)' --outfolder output/seq10_test2
  [ "$status" -eq 0 ]
}

@test "Syntax: Fit zero layers balance, reg l1_l2" {
  rm -r -f output/seq10_test3
  run lbsnn-fit -x data/seq10.npy -y output/seq10.npy -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' -br 'l1_l2(0.01)' --balance --outfolder output/seq10_test3
  [ "$status" -eq 0 ]
}

@test "Syntax: Fit zero layers shuffle, batch_size" {
  rm -r -f output/seq10_test4
  run lbsnn-fit -x data/seq10.npy -y output/seq10.npy -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' -br 'l1_l2(0.01)' --balance --shuffle -bs 2 --outfolder output/seq10_test4
  [ "$status" -eq 0 ]
}

@test "Syntax: Fit one layer" {
  rm -r -f output/seq10_test5
  run lbsnn-fit -x data/seq10.npy -y output/seq10.npy -l 1 -u 32 -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' -br 'l1_l2(0.01)' --balance --shuffle -bs 2 --outfolder output/seq10_test5
  [ "$status" -eq 0 ]
}

@test "Syntax: Fit two layers" {
  rm -r -f output/seq10_test6
  run lbsnn-fit -x data/seq10.npy -y output/seq10.npy -l 2 -u 32 45 -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' 'l2(0.01)' 'l1(0.0001)' -br 'l1_l2(0.01)' --balance --shuffle -bs 2 --outfolder output/seq10_test6
  [ "$status" -eq 0 ]
}

@test "Syntax: Fit multi layers, es and file creation" {
  rm -r -f output/seq10_test7
  run lbsnn-fit -x data/seq10.npy -y output/seq10.npy -l 10 -u 32 -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' -br 'l1_l2(0.01)' --balance --shuffle -bs 2 -es 2 --outfolder output/seq10_test7 
  [ "$status" -eq 0 ]
  [ -f output/seq10_test7/args.0.json ]
  [ -f output/seq10_test7/model.0.h5 ]
  [ -f output/seq10_test7/traininglog.0.csv ]
  [ -f output/seq10_test7/sampleweights.0.txt ]
}

@test "Syntax: Expand all" {
  run lbsnn-expand -m output/seq10_test1/model.0.h5 -p data/seq10.profile -2 output/seq10_test1/second.npy -1 output/seq10_test1/first.npy -0 output/seq10_test1/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test2/model.0.h5 -p data/seq10.profile -2 output/seq10_test2/second.npy -1 output/seq10_test2/first.npy -0 output/seq10_test2/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test3/model.0.h5 -p data/seq10.profile -2 output/seq10_test3/second.npy -1 output/seq10_test3/first.npy -0 output/seq10_test3/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test4/model.0.h5 -p data/seq10.profile -2 output/seq10_test4/second.npy -1 output/seq10_test4/first.npy -0 output/seq10_test4/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test5/model.0.h5 -p data/seq10.profile -2 output/seq10_test5/second.npy -1 output/seq10_test5/first.npy -0 output/seq10_test5/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test6/model.0.h5 -p data/seq10.profile -2 output/seq10_test6/second.npy -1 output/seq10_test6/first.npy -0 output/seq10_test6/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test7/model.0.h5 -p data/seq10.profile -2 output/seq10_test7/second.npy -1 output/seq10_test7/first.npy -0 output/seq10_test7/zeroth.npy
  [ "$status" -eq 0 ]
}

@test "Syntax: Score all" {
  run lbsnn-score -px data/seq10.profile -py output/seq10.profile -2 output/seq10_test1/second.npy -o output/seq10_test1/score_t.txt
  [ "$status" -eq 0 ]
  run lbsnn-score -px data/seq10.profile -py output/seq10.profile -2 output/seq10_test2/second.npy -1 output/seq10_test2/first.npy -o output/seq10_test2/score_s.txt --ising-gauge
  [ "$status" -eq 0 ]
  run lbsnn-score -px data/seq10.profile -py output/seq10.profile -2 output/seq10_test2/second.npy -o output/seq10_test2/score_t.txt --ising-gauge --sym --apc
  [ "$status" -eq 0 ]
}
