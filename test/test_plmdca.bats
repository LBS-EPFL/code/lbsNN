#!/usr/bin/env bats

@test "Syntax: plmDCA second order" {
  rm -r -f output/seq10_test0
  run lbsnn-plmdca -x data/seq10.npy -ep 10 --learning-rate 0.1 -p data/seq10.profile --outfolder output/seq10_test0
  [ "$status" -eq 0 ]
}

@test "Syntax: plmDCA zero layers noreg" {
  rm -r -f output/seq10_test1
  run lbsnn-plmdca -x data/seq10.npy -ep 10 --learning-rate 0.1 -p data/seq10.profile --outfolder output/seq10_test1
  [ "$status" -eq 0 ]
}

@test "Syntax: plmDCA zero layers reg l1 l2" {
  rm -r -f output/seq10_test2
  run lbsnn-plmdca -x data/seq10.npy -ep 10 --learning-rate 0.1 -kr 'l1(0.1)' -br 'l2(0.01)' -p data/seq10.profile --outfolder output/seq10_test2
  [ "$status" -eq 0 ]
}

@test "Syntax: plmDCA zero layers balance, reg l1_l2" {
  rm -r -f output/seq10_test3
  run lbsnn-plmdca -x data/seq10.npy -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' -br 'l1_l2(0.01)' --balance -p data/seq10.profile --outfolder output/seq10_test3
  [ "$status" -eq 0 ]
}

@test "Syntax: plmDCA zero layers shuffle, batch_size" {
  rm -r -f output/seq10_test4
  run lbsnn-plmdca -x data/seq10.npy -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' -br 'l1_l2(0.01)' --balance --shuffle -bs 2 -p data/seq10.profile --outfolder output/seq10_test4
  [ "$status" -eq 0 ]
}

@test "Syntax: plmDCA one layer" {
  rm -r -f output/seq10_test5
  run lbsnn-plmdca -x data/seq10.npy -l 1 -u 32 -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' -br 'l1_l2(0.01)' --balance --shuffle -bs 2 -p data/seq10.profile --outfolder output/seq10_test5
  [ "$status" -eq 0 ]
}

@test "Syntax: plmDCA two layers" {
  rm -r -f output/seq10_test6
  run lbsnn-plmdca -x data/seq10.npy -l 2 -u 32 45 -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' 'l2(0.01)' 'l1(0.0001)' -br 'l1_l2(0.01)' --balance --shuffle -bs 2 -p data/seq10.profile --outfolder output/seq10_test6
  [ "$status" -eq 0 ]
}

@test "Syntax: plmDCA multi layers, es and file creation" {
  rm -r -f output/seq10_test7
  run lbsnn-plmdca -x data/seq10.npy -l 10 -u 32 -ep 10 --learning-rate 0.1 -kr 'l1_l2(0.1)' -br 'l1_l2(0.01)' --balance --shuffle -bs 2 -es 2 -p data/seq10.profile --outfolder output/seq10_test7 
  [ "$status" -eq 0 ]
  [ -f output/seq10_test7/args.0.json ]
  [ -f output/seq10_test7/model_col10.0.h5 ]
  [ -f output/seq10_test7/traininglog_col10.0.csv ]
  [ -f output/seq10_test7/sampleweights.0.txt ]
}

@test "Syntax: plmDCA multi layers, rerun and file creation" {
  rm -r -f output/seq10_test8
  run lbsnn-plmdca --from-json output/seq10_test7/args.0.json --outfolder output/seq10_test8 
  [ "$status" -eq 0 ]
  [ -f output/seq10_test8/args.0.json ]
  [ -f output/seq10_test8/model_col10.0.h5 ]
  [ -f output/seq10_test8/traininglog_col10.0.csv ]
  [ -f output/seq10_test8/sampleweights.0.txt ]
}
@test "Syntax: Expand all" {
  run lbsnn-expand -m output/seq10_test0 -p data/seq10.profile -1 output/seq10_test0/first.npy 
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test1 -p data/seq10.profile -1 output/seq10_test1/first.npy -0 output/seq10_test1/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test2 -p data/seq10.profile -1 output/seq10_test2/first.npy -0 output/seq10_test2/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test3 -p data/seq10.profile -1 output/seq10_test3/first.npy -0 output/seq10_test3/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test4 -p data/seq10.profile -1 output/seq10_test4/first.npy -0 output/seq10_test4/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test5 -p data/seq10.profile -1 output/seq10_test5/first.npy -0 output/seq10_test5/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test6 -p data/seq10.profile -1 output/seq10_test6/first.npy -0 output/seq10_test6/zeroth.npy
  [ "$status" -eq 0 ]
  run lbsnn-expand -m output/seq10_test7 -p data/seq10.profile -1 output/seq10_test7/first.npy
  [ "$status" -eq 0 ]
}

@test "Syntax: Score some" {
  run lbsnn-score -px data/seq10.profile -py data/seq10.profile -1 output/seq10_test0/first.npy -o output/seq10_test0/score_s.txt
  [ "$status" -eq 0 ]
  run lbsnn-score -px data/seq10.profile -py data/seq10.profile -1 output/seq10_test1/first.npy -1 output/seq10_test1/first.npy -o output/seq10_test1/score_s.txt
  [ "$status" -eq 0 ]
  run lbsnn-score -px data/seq10.profile -py data/seq10.profile -1 output/seq10_test1/first.npy -o output/seq10_test1/score_t.txt
  [ "$status" -eq 0 ]
  run lbsnn-score -px data/seq10.profile -py data/seq10.profile -1 output/seq10_test2/first.npy -1 output/seq10_test2/first.npy -o output/seq10_test2/score_s.txt --ising-gauge
  [ "$status" -eq 0 ]
  run lbsnn-score -px data/seq10.profile -py data/seq10.profile -1 output/seq10_test2/first.npy -o output/seq10_test2/score_t.txt --ising-gauge --sym --apc
  [ "$status" -eq 0 ]
}
