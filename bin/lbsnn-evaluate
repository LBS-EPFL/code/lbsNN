#!/usr/bin/env python3

import argparse
import tensorflow as tf
from tensorflow.keras.models import load_model,Model
from lbsNN.models import evaluate,register_custom_objects
from lbsNN.log import setup_logging
from tabulate import tabulate

import numpy as np

if __name__=="__main__":
  parser = argparse.ArgumentParser(prog='lbsnn-evaluate',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  # backend
  setup_group = parser.add_argument_group('Setup environment')
  setup_group.add_argument('-loglevel', dest='loglevel', type=str, choices=["DEBUG","INFO","WARNING","ERROR","CRITICAL"], default="ERROR", help='Logging: log level')
  setup_group.add_argument('-logfile', dest='logfile', type=str, default=None, help='Logging: log file. Default: print log to stdout')
  # data
  data_group = parser.add_argument_group('Data')
  data_group.add_argument('-x', dest='x', type=str,required=True, help='Data')
  data_group.add_argument('-y', dest='y', type=str,required=True, help='Target class/label')
  data_group.add_argument('--sample-weights', dest='sample_weights', type=str, help='Reweight each input data')
  # network description
  network_group = parser.add_argument_group('Network')
  network_group.add_argument('-m','--model', type=str, required=True, nargs='+', help="Model")
  network_group.add_argument('-bs', '--batch-size', dest='batch_size', type=int, default=1024, help='Batch size')
  network_group.add_argument('--input-layer', dest='input_layer', type=str, default='input', help='Input layer')
  network_group.add_argument('--output-layer', dest='output_layer', type=str, default='output', help='Output layer')
  # output
  output_group = parser.add_argument_group('Output')
  output_group.add_argument('-fmt','--tablefmt', dest='tablefmt', type=str, default='plain', help='Output format with stored metrics')
  output_group.add_argument('--sort-by', dest='sortby', type=str, default='accuracy', help='Sort output in increasing order of metric')
  output_group.add_argument('-o','--out-file', dest='outfile', type=str, default=None, help='Output file with stored predictions')
  
  # info
  info_group = parser.add_argument_group('About')
  info_group.add_argument('--version', action='version', version='%(prog)s 3.0')
  # parse commandline arguments
  args=parser.parse_args()

  # setup environment
  setup_logging(args.loglevel,args.logfile)
  register_custom_objects()
  
  # load input data
  x=np.load(args.x).astype(np.float16)
  y=np.load(args.y).astype(np.float16)
  if args.sample_weights is None:
    sw=np.ones((x.shape[0],))
  else:
    sw=np.loadtxt(args.sample_weights)
    
  # load the model
  reports=[]
  for modelfile in args.model:
    model=load_model(modelfile)
    
    inputs=model.get_layer(args.input_layer).output
    output=model.get_layer(args.output_layer).output
    model=Model(inputs,output)
    metrics=[
        tf.keras.metrics.CategoricalAccuracy(name='accuracy'),
        tf.keras.metrics.TruePositives(name='tp'),
        tf.keras.metrics.FalsePositives(name='fp'),
        tf.keras.metrics.TrueNegatives(name='tn'),
        tf.keras.metrics.FalseNegatives(name='fn'), 
        tf.keras.metrics.Precision(name='precision'),
        tf.keras.metrics.Recall(name='recall'),
        tf.keras.metrics.AUC(name='auc')]
    model.compile('Adam','categorical_crossentropy',metrics=metrics)
    report=evaluate(model,x,y,sample_weights=sw,batch_size=args.batch_size)
    reports.append([modelfile]+report)
  header=['filename']+model.metrics_names
  reports=sorted(reports,key=lambda x,h=header,a=args : x[ header.index(args.sortby) ] )
  with open(args.outfile,'w') as outfile:
    outfile.write(tabulate(reports,headers=header,tablefmt=args.tablefmt))
