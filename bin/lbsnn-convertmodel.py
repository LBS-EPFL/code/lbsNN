#!/usr/bin/env python3

import argparse
import tensorflow as tf
from lbsNN.models import build_second_order_network,build_sequential_classifier,train, get_optimizer,get_callbacks,set_random_seed,register_custom_objects
from lbsNN.parsers import str2regularizer,str2bool,str2prob
from lbsNN.preprocess import rebalance
from lbsNN.storage import setup_paths,save_cmd_args
from lbsNN.log import setup_logging
import time
import numpy as np
import json

if __name__=="__main__":
  parser = argparse.ArgumentParser(prog='lbsnn-convertmodel',formatter_class=argparse.ArgumentDefaultsHelpFormatter,add_help=False)
  parser.add_argument('--from-json', type=argparse.FileType('r'),help='Load default parameters from a json file')
  args, remaining = parser.parse_known_args()
  defaults = {}
  if args.from_json:
    try:
      defaults = json.load(args.from_json)
    except ValueError:
      raise ValueError('Error: Could not parse json file', file=sys.stderr)
    finally:
      args.from_json.close()
  parser.add_argument('-h','--help', action='help',help='Print this help message')
  # backend
  setup_group = parser.add_argument_group('Setup environment')
  setup_group.add_argument('--seed', dest='seed', type=int,default=int(round(time.time())),help='seed')
  setup_group.add_argument('-N','--name', dest='jobname', type=str,default="", help='Task name or identifier')
  setup_group.add_argument('-loglevel', dest='loglevel', type=str, choices=["DEBUG","INFO","WARNING","ERROR","CRITICAL"], default="ERROR", help='Logging: log level')
  setup_group.add_argument('-logfile', dest='logfile', type=str, default=None, help='Logging: log file. Default: print log to stdout')
  setup_group.add_argument('-verbose', dest='verbose', action='store_true', default=False, help='Print information during training')  
  # data
  data_group = parser.add_argument_group('Data')
  data_group.add_argument('-x', dest='x', type=str,required=('x' not in defaults), help='Data')
  data_group.add_argument('-y', dest='y', type=str,required=('y' not in defaults), help='Target class/label')
  # network description
  network_group = parser.add_argument_group('Network')
  network_group.add_argument('-l','--num-layers', type=int, default=0,help="Number of hidden layers")
  network_group.add_argument('-u','--num-units', type=int,nargs='+', default=[64],help="Units per layer")
  network_group.add_argument('-a','--activation', type=str,nargs='+', default=["softplus"],help="Activation function per layer")
  network_group.add_argument('-br','--bias-regularizer', type=str,nargs='+', default=[None],help="Bias regularizer per layer")
  network_group.add_argument('-kr','--kernel-regularizer', type=str,nargs='+', default=[None],help="Kernel regularizer per layer")
  network_group.add_argument('-ub','--use-bias', type=str,nargs='+', default=[True],help="Decide whether to use biases or no")
  network_group.add_argument('--dropout', type=str, default=0.,help="Dropout value")
  network_group.add_argument('--initial-weights', type=str, default=None,help="Initial parameters")
  # output
  output_group = parser.add_argument_group('Output')
  output_group.add_argument('--outfolder', dest='outfolder', type=str, default=None, help='Folder where to save generated files')
  # info
  info_group = parser.add_argument_group('About')
  info_group.add_argument('--version', action='version', version='%(prog)s 3.0')
  # parse commandline arguments
  parser.set_defaults(**defaults)
  args=parser.parse_args(remaining)

  # setup environment
  filename=setup_paths(args.outfolder,args.y,args.jobname,0)
  setup_logging(args.loglevel,args.logfile)
  set_random_seed(args.seed)
  register_custom_objects()
  save_cmd_args(args,filename['args']+'.json')
  args.kernel_regularizer=[str2regularizer(kr) for kr in args.kernel_regularizer]
  args.bias_regularizer=[str2regularizer(br) for br in args.bias_regularizer]
  args.use_bias=[str2bool(ub) for ub in args.use_bias]
  args.dropout=str2prob(args.dropout)


  # load training data
  x=np.load(args.x).astype(np.float16)
  y=np.load(args.y).astype(np.float16)
  if args.sample_weights is None:
    sw=np.ones((x.shape[0],))
  else:
    sw=np.loadtxt(args.sample_weights)

  # build the model
  model=build_sequential_classifier(x.shape[1],y.shape[1],args.num_layers,args.num_units,args.activation, \
        args.kernel_regularizer,args.bias_regularizer,args.use_bias,dropout=args.dropout,seed=args.seed, \
        weights_filename=args.initial_weights)
  
  model.load_weights(filename["model"]+".h5")
  model.save(filename["model"]+"_resaved.h5")
