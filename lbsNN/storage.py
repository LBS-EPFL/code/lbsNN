import numpy as np
import os
import json



def setup_paths(outfolder,yfilename,jobname,init_idx=None):
  def get_basename(outfolder,filename,jobname):
    if len(jobname):
      filename=filename+"_{}".format(jobname)
    if outfolder[-1]!="/":
      outfolder+="/"
    return outfolder+filename
    
  if outfolder is None:
    outfolder=".".join( yfilename.split('.')[:-1] )+"/"
  if not os.path.isdir(outfolder):
    os.makedirs(outfolder)
  if init_idx is None:
    idx=0
    while os.path.isfile( get_basename(outfolder,"args","")+".{}.json".format(idx) ):
      idx+=1
    if len(jobname):
      idx-=1
    init_idx=idx
  filenames=["args","model","sampleweights","checkpoint","traininglog","couplings","average"]
  return {f:get_basename(outfolder,f,jobname)+".{}".format(init_idx) for f in filenames}

def save_cmd_args(args,filename):
  from tensorflow.keras.regularizers import L1L2
  from lbsNN.models import l1_l2_fluxes
  class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
      if isinstance(obj, L1L2):
        return obj.get_config()
      if isinstance(obj, l1_l2_fluxes):
        return obj.get_config()
      return json.JSONEncoder.default(self, obj)

  with open(filename,'w') as jsonargs:
    jsonargs.write("{}\n".format(json.dumps(vars(args), indent=4, sort_keys=True, cls = CustomEncoder)))

def save_score(filename,score):
  if score.ndim==2 and score.shape[0]==score.shape[1]:
    from scipy.spatial.distance import squareform
    SS=np.triu(score) + np.triu(score).T -2*np.diag(np.diagonal(score))
    SS=squareform(SS)
    np.savetxt(filename,SS)
  else:
    np.savetxt(filename,score)
