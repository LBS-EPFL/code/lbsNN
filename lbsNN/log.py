import tensorflow as tf
import logging
def setup_logging(loglevel,logfile):
  tf.get_logger().setLevel(loglevel)
  numeric_level = getattr(logging, loglevel.upper(), None)
  if not isinstance(numeric_level, int):
    raise ValueError('Invalid log level: %s' % loglevel)
  if logfile is None:
    logging.basicConfig(level=numeric_level)
  else:
    logging.basicConfig(filename=logfile,level=numeric_level)
