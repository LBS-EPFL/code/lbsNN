import tensorflow as tf
from tensorflow.keras.models import load_model,Model
from lbsNN.models import get_submodel
import numpy as np
import glob
import os


def expand_network_first_order(model,cwidth,batch_size=1024):
  if model.name=='second_order_network':
    s=model.get_layer('energy').get_weights()[0]
    f=np.zeros((s.shape[0],))
    return f,s
  else:
    seq_0=np.array([np.zeros(shape=(cwidth[-1]),dtype=np.float16)])
    seq_1=np.eye(cwidth[-1],dtype=np.float16)
    params_0=model.predict(seq_0,batch_size=batch_size)
    params_1=model.predict(seq_1)
    params_0=np.reshape(params_0,(params_0.shape[1],))
  return params_0,(params_1-params_0).T

def expand_network_second_order(model,cwidth,batch_size=1024):
  if model.name=='second_order_network':
    f,s=expand_network_first_order(model,cwidth,batch_size)
    return f,s,None
  else:
    f,s=expand_network_first_order(model,cwidth,batch_size)
    
    Ncols=cwidth[-1]
    indexes=[]
    params=[]
    for idx,c in enumerate(cwidth[:-1]):
      x=list(range(c,cwidth[idx+1]))
      y=list(range(cwidth[idx+1],Ncols))
      indexes.extend( list(np.array(np.meshgrid(x,y)).T.reshape(-1,2)) )
    for step in range(0, len(indexes), batch_size):
      seq=[]
      for index in indexes[step:step+batch_size]:
        a=np.zeros(shape=(Ncols),dtype=np.float16)
        a[list(index)]=1.
        seq.append(a)
      params.extend(list(model.predict(np.array(seq))))
    
    indexes=np.array(indexes).T
    params=np.array(params).T
    params-=( s[:,list(indexes[0])] + s[:,list(indexes[1])] + f[:,np.newaxis] )
    
    t=np.zeros((s.shape[0],Ncols,Ncols))
    t[ :,indexes[0],indexes[1] ]=params
    t[ :,indexes[1],indexes[0] ]=params
  return f,s,t

def expand_network_plmdca(folder,outlayer,cwidth,jobid):
  def remove_from_cumulative(cwidth,idx):
    cumwidth=[cw for cw in cwidth]
    w=cumwidth[idx+1]-cumwidth[idx]
    for i in range(idx+1,len(cumwidth)):
      cumwidth[i]-=w
    del cumwidth[idx+1]
    return cumwidth
  
  files=glob.glob(os.path.join(folder,"model_col*.h5"))
  N=2*len(files)
  if N==0:
    raise RuntimeError("Error: no valid DCA model found in {}".format(folder))
  
  ord_files=[]
  for n in range(N):
    if os.path.join(folder,"model_col{}.{}.h5".format(n,jobid)) in files:
      ord_files.append( os.path.join(folder,"model_col{}.{}.h5".format(n,jobid)) )
    else:
      ord_files.append("")
  
  while ord_files[0]=="":
    ord_files.pop(0)
  while ord_files[-1]=="":
    ord_files.pop()
  
  model=get_submodel(ord_files[0],'input',outlayer)
  fields,first=expand_network_first_order( model,remove_from_cumulative(cwidth,0) )
  
  nrows,ncols=first.shape
  N=nrows+ncols
  
  F=np.zeros((N,))
  S=np.zeros((N,N))

  row=0
  col=0
  S[row:row+nrows,0:row]=first[:,:row]
  S[row:row+nrows,row+nrows:]=first[:,row:]
  F[row:row+nrows]=fields
  row+=nrows
  col+=ncols
  
  for n in range(1,len(ord_files)):
    if ord_files[n]=="":
      col+=1
      continue
    model=get_submodel(ord_files[n],'input',outlayer)
    fields,first=expand_network_first_order( model,remove_from_cumulative(cwidth,n) )
    nrows,ncols=first.shape
    S[row:row+nrows,0:row]=first[:,:row]
    S[row:row+nrows,row+nrows:]=first[:,row:]
    F[row:row+nrows]=fields
    row+=nrows
    col+=ncols
  return F,S

def expand_network_plmdca_second_order(folder,outlayer,cwidth,jobid):
  def remove_from_cumulative(cwidth,idx):
    cumwidth=[cw for cw in cwidth]
    w=cumwidth[idx+1]-cumwidth[idx]
    for i in range(idx+1,len(cumwidth)):
      cumwidth[i]-=w
    del cumwidth[idx+1]
    return cumwidth
  
  files=glob.glob(os.path.join(folder,"model_col*.h5"))
  N=2*len(files)
  if N==0:
    raise RuntimeError("Error: no valid DCA model found in {}".format(folder))
  
  ord_files=[]
  for n in range(N):
    if os.path.join(folder,"model_col{}.{}.h5".format(n,jobid)) in files:
      ord_files.append( os.path.join(folder,"model_col{}.{}.h5".format(n,jobid)) )
    else:
      ord_files.append("")
  
  while ord_files[0]=="":
    ord_files.pop(0)
  while ord_files[-1]=="":
    ord_files.pop()
  
  model=get_submodel(ord_files[0],'input',outlayer)
  fields,first,second=expand_network_second_order( model,remove_from_cumulative(cwidth,0) )

  nrows,ncols=first.shape
  N=nrows+ncols
  
  F=np.zeros((N,))
  S=np.zeros((N,N))
  T=np.zeros((N,N,N),dtype=np.float16)

  row=0
  col=0
  T=np.zeros((second.shape[0],N,N))
  T[:,0:row,0:row]=second[:,:row,:row]
  T[:,row+nrows:,0:row]=second[:,row:,:row]
  T[:,0:row,row+nrows:]=second[:,:row,row:]
  T[:,row+nrows:,row+nrows:]=second[:,row:,row:]
  S[row:row+nrows,0:row]=first[:,:row]
  S[row:row+nrows,row+nrows:]=first[:,row:]
  F[row:row+nrows]=fields
  row+=nrows
  col+=ncols

  for n in range(1,len(ord_files)):
    if ord_files[n]=="":
      col+=1
      continue
    model=get_submodel(ord_files[n],'input',outlayer)
    fields,first,second=expand_network_second_order( model,remove_from_cumulative(cwidth,0) )
    nrows,ncols=first.shape
    T[row:row+nrows,0:row,0:row]=second[:,:row,:row]
    T[row:row+nrows,row+nrows:,0:row]=second[:,row:,:row]
    T[row:row+nrows,0:row,row+nrows:]=second[:,:row,row:]
    T[row:row+nrows,row+nrows:,row+nrows:]=second[:,row:,row:]
    S[row:row+nrows,0:row]=first[:,:row]
    S[row:row+nrows,row+nrows:]=first[:,row:]
    F[row:row+nrows]=fields
    row+=nrows
    col+=ncols
  return F,S,T

def score_network_plmdca_second_order(folder,outlayer,cwidth,jobid):
  def remove_from_cumulative(cwidth,idx):
    cumwidth=[cw for cw in cwidth]
    w=cumwidth[idx+1]-cumwidth[idx]
    for i in range(idx+1,len(cumwidth)):
      cumwidth[i]-=w
    del cumwidth[idx+1]
    return cumwidth
  
  files=glob.glob(os.path.join(folder,"model_col*.h5"))
  N=2*len(files)
  if N==0:
    raise RuntimeError("Error: no valid DCA model found in {}".format(folder))
  
  ord_files=[]
  for n in range(N):
    if os.path.join(folder,"model_col{}.{}.h5".format(n,jobid)) in files:
      ord_files.append( os.path.join(folder,"model_col{}.{}.h5".format(n,jobid)) )
    else:
      ord_files.append("")
  
  while ord_files[0]=="":
    ord_files.pop(0)
  while ord_files[-1]=="":
    ord_files.pop()
  
  model=get_submodel(ord_files[0],'input',outlayer)
  fields,first,second=expand_network_second_order( model,remove_from_cumulative(cwidth,0) )
  print("######### Second: {}".format(second.shape))
  fields,first,second=ising_gauge([fields,first,second],[0,first.shape[0]],remove_from_cumulative(cwidth,0))
  norm=norm_score(second,[0,first.shape[0]],remove_from_cumulative(cwidth,0))
  print("######### Norm: {}".format(norm.shape))

  aa=len(cwidth)-1
  T=np.zeros((aa,aa,aa))
  nrows,ncols=first.shape
  N=nrows+ncols
  F=np.zeros((N,))
  S=np.zeros((N,N))
  row=0
  col=0
  
  
  T[0,1:,1:]=norm
  S[row:row+nrows,0:row]=first[:,:row]
  S[row:row+nrows,row+nrows:]=first[:,row:]
  F[row:row+nrows]=fields
  row+=nrows
  col+=ncols

  for n in range(1,len(ord_files)):
    if ord_files[n]=="":
      col+=1
      continue
    model=get_submodel(ord_files[n],'input',outlayer)
    fields,first,second=expand_network_second_order( model,remove_from_cumulative(cwidth,n) )
    print("######### Second: {}".format(second.shape))
    fields,first,second=ising_gauge([fields,first,second],[0,first.shape[0]],remove_from_cumulative(cwidth,n))
    norm=norm_score(second,[0,first.shape[0]],remove_from_cumulative(cwidth,n))
    print("######### Norm: {} n: {}".format(norm.shape,n))

    nrows,ncols=first.shape
    T[n,:n,:n]=norm[0,:n,:n]
    T[n,n+1:,:n]=norm[0,n:,:n]
    T[n,:n,n+1:]=norm[0,:n,n:]
    T[n,n+1:,n+1:]=norm[0,n:,n:]
    S[row:row+nrows,0:row]=first[:,:row]
    S[row:row+nrows,row+nrows:]=first[:,row:]
    F[row:row+nrows]=fields
    row+=nrows
    col+=ncols
  F,S=ising_gauge([F,S],cwidth,cwidth)
  S=norm_score(S,cwidth,cwidth)
  return S,T

def norm_score(matrix,cwidth0,cwidth1,norm_t='fro',keepdims=True):
  if matrix.ndim==3:
    score=np.zeros( (len(cwidth0)-1,len(cwidth1)-1,len(cwidth1)-1) )
    for x in range(len(cwidth0)-1):
      for y in range(len(cwidth1)-1):
        for z in range(y+1,len(cwidth1)-1):
          score[x,y,z]=np.linalg.norm(matrix[ cwidth0[x]:cwidth0[x+1],cwidth1[y]:cwidth1[y+1],cwidth1[z]:cwidth1[z+1] ])
          score[x,z,y]=np.linalg.norm(matrix[ cwidth0[x]:cwidth0[x+1],cwidth1[z]:cwidth1[z+1],cwidth1[y]:cwidth1[y+1] ])
    score+=np.transpose(score,(0,2,1))
    score/=2.
  elif matrix.ndim==2:
    score=np.zeros( (len(cwidth0)-1,len(cwidth1)-1) )
    for x in range(len(cwidth0)-1):
      for y in range(len(cwidth1)-1):
        score[x,y]=np.linalg.norm(matrix[ cwidth0[x]:cwidth0[x+1],cwidth1[y]:cwidth1[y+1] ],ord=norm_t)
  elif matrix.ndim==1:
    score=np.zeros( (len(cwidth0)-1,) )
    for x in range(len(cwidth0)-1):
      score[x]=np.linalg.norm(matrix[ cwidth0[x]:cwidth0[x+1] ])
  else:
    raise ValueError("norm() can only be computed for matrices of up to 3 dimensions.")
  if keepdims:
    return score
  else:
    return np.squeeze(score)
        
def ising_gauge(matrices,cwidth0,cwidth1):
  if len(matrices)>3:
    raise ValueError("ising_gauge() can be computed for matrices of up to 3 dimensions.")
  if matrices[0].ndim!=1:
    raise ValueError("The first matrix to be passed to ising_gauge() must be of dimension 1.")
  for i in range(1,len(matrices)):
    if matrices[i].ndim!=(matrices[i-1].ndim+1):
      raise ValueError("The n-th matrix to be passed to ising_gauge() must be of dimension n.")
  
  if len(matrices)==3:
    for x in range(len(cwidth0)-1):
      for y in range(len(cwidth1)-1):
        for z in range(len(cwidth1)-1):
          block = matrices[2][ cwidth0[x]:cwidth0[x+1],cwidth1[y]:cwidth1[y+1],cwidth1[z]:cwidth1[z+1] ]
          c=np.mean(block,keepdims=True)
          cx=np.mean(block,axis=0,keepdims=True)
          cy=np.mean(block,axis=1,keepdims=True)
          cz=np.mean(block,axis=2,keepdims=True)
          cxy=np.mean(cx,axis=1,keepdims=True)
          cxz=np.mean(cx,axis=2,keepdims=True)
          cyz=np.mean(cy,axis=2,keepdims=True)
          
          s=block-cx-cy-cz+cxy+cxz+cyz-c
          matrices[2][ cwidth0[x]:cwidth0[x+1],cwidth1[y]:cwidth1[y+1],cwidth1[z]:cwidth1[z+1] ] = s
          # matrices[2][ cwidth0[x]:cwidth0[x+1],cwidth1[z]:cwidth1[z+1],cwidth1[y]:cwidth1[y+1] ] = np.transpose(s,(0,2,1))
          matrices[1][ cwidth0[x]:cwidth0[x+1],cwidth1[y]:cwidth1[y+1]] += cz[:,:,0]
          matrices[1][ cwidth0[x]:cwidth0[x+1],cwidth1[z]:cwidth1[z+1]] += cy[:,0,:]
          matrices[0][ cwidth0[x]:cwidth0[x+1]] -= np.squeeze(cyz)
    matrices[0],matrices[1] = ising_gauge( matrices[:-1],cwidth0,cwidth1 )[0:2]
  elif len(matrices)==2:
    for x in range(len(cwidth0)-1):
      for y in range(len(cwidth1)-1):
        block=matrices[1][ cwidth0[x]:cwidth0[x+1],cwidth1[y]:cwidth1[y+1] ]
        c=np.mean(block,keepdims=True)
        cx=np.mean(block,axis=0,keepdims=True)
        cy=np.mean(block,axis=1,keepdims=True)

        matrices[1][ cwidth0[x]:cwidth0[x+1],cwidth1[y]:cwidth1[y+1]]=block-cx-cy+c
        matrices[0][ cwidth0[x]:cwidth0[x+1]]+=np.squeeze(cy)
    matrices[0] = ising_gauge([matrices[0]],cwidth0,cwidth1)[0]
  
  elif len(matrices)==1:
    for x in range(len(cwidth0)-1):
      block=matrices[0][ cwidth0[x]:cwidth0[x+1] ]
      c=np.mean(block,keepdims=True)
      matrices[0][ cwidth0[x]:cwidth0[x+1]]=block-c
  
  return matrices

def apc(matrix):
  return matrix-np.outer(matrix.mean(axis=0),matrix.mean(axis=0))/np.mean(matrix)