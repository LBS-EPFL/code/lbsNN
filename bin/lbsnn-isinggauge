#!/usr/bin/env python3

import argparse
from lbsNN.log import setup_logging
from lbsNN.preprocess import parse_profile,profile_data
from lbsNN.postprocess import norm_score,ising_gauge,apc
from scipy.spatial.distance import squareform
import numpy as np

if __name__=="__main__":
  parser = argparse.ArgumentParser(prog='lbsnn-isinggauge',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  # backend
  setup_group = parser.add_argument_group('Setup environment')
  setup_group.add_argument('-loglevel', dest='loglevel', type=str, choices=["DEBUG","INFO","WARNING","ERROR","CRITICAL"], default="ERROR", help='Logging: log level')
  setup_group.add_argument('-logfile', dest='logfile', type=str, default=None, help='Logging: log file. Default: print log to stdout')
  # data
  data_group = parser.add_argument_group('Data')
  data_group.add_argument('-px', dest='profile_x', type=str,required=True,default=None, help='Profile used for encoding data')
  data_group.add_argument('-py', dest='profile_y', type=str,required=True,default=None, help='Profile used for encoding labels')
  data_group.add_argument('-2','--in-second-order', dest='infile_third', type=str, default=None, help='Input file with stored second-order expansion')
  data_group.add_argument('-1','--in-first-order', dest='infile_second', type=str, default=None, help='Input file with stored first-order expansion')
  data_group.add_argument('-0','--in-zeroth-order', dest='infile_first', type=str, default=None, help='Input file with stored zeroth-order expansion')
  # info
  info_group = parser.add_argument_group('About')
  info_group.add_argument('--version', action='version', version='%(prog)s 3.0')
  # parse commandline arguments
  args=parser.parse_args()
  
  cumwidth_x=profile_data(parse_profile(args.profile_x)).cumulative
  cumwidth_y=profile_data(parse_profile(args.profile_y)).cumulative
  

  orders=[]
  compute_order=0
  if args.infile_first is not None:
    f=np.load(args.infile_first)
    orders.append(f)
    compute_order=1
  else:
    orders.append(np.zeros((cumwidth_y[-1],)))
  
  if args.infile_second is not None:
    s=np.load(args.infile_second)
    orders.append(s)
    if compute_order==0:
      compute_order=2
  elif args.infile_third is not None:
    orders.append(np.zeros((cumwidth_y[-1],cumwidth_x[-1])))
  
  if args.infile_third is not None:
    s=np.load(args.infile_third)
    orders.append(s)
    if compute_order==0:
      compute_order=3
  
  orders=ising_gauge(orders,cumwidth_y,cumwidth_x)
  ll=0
  if args.infile_first is not None:
    np.save(args.infile_first.replace(".npy","_isingauge.npy"),orders[ll])
    ll+=1
  if args.infile_second is not None:
    np.save(args.infile_second.replace(".npy","_isingauge.npy"),orders[ll])
    ll+=1
  if args.infile_third is not None:
    np.save(args.infile_third.replace(".npy","_isingauge.npy"),orders[ll])
