import argparse
from tensorflow.keras.regularizers import l1,l2,l1_l2

def str2bool(v):
  if isinstance(v, bool):
    return v
  if v.lower() in ('yes', 'true', 't', 'y', '1'):
    return True
  elif v.lower() in ('no', 'false', 'f', 'n', '0'):
    return False
  else:
    raise argparse.ArgumentTypeError('Boolean value expected.')

def str2prob(v):
  try:
    f=float(v)
  except:
    raise argparse.ArgumentTypeError('Float value expected.')
  if f<0. or f>1.:
    raise argparse.ArgumentTypeError('A value between zero and one was expected')
  return f

def str2regularizer(v):
  if v is None:
    return None
  args=v[v.find("(")+1:v.find(")")].split(';')
  if 'l1l2' in v and len(args)==2:
    return l1_l2(float(args[0]),float(args[1]))
  if 'l1' in v:
    return l1(float(args[0]))
  if 'l2' in v:
    return l2(float(args[0]))
  else:
    raise argparse.ArgumentTypeError('Regularizer type must be one of the following: l1, l2 or l1l2')
