from collections import defaultdict
import numpy as np
import sys  

## Data balancing ##
def rebalance(y,w):
  d=y*w[:,np.newaxis]
  d/=d.sum(axis=0,keepdims=True)
  return d.sum(axis=1)/y.shape[1]*w.sum()

## Profile ##
def parse_profile(infile):
  profile=[]
  with open(infile,'r') as freqfile:
    for line in freqfile:
      line=line.strip()
      if line[0]=="#":
        continue
      freq=parse_profile_line(line)
      profile.append(freq)
  return profile

def parse_profile_line(profileline):
  tok=profileline.split()
  freq=dict(d.split(':') for d in tok)
  return defaultdict(lambda:0, {key:float(val) for key,val in freq.items()})

class profile_data:
  def __init__(self,profile):
    self.uniq=[list(col.keys()) for col in profile]
    self.cumulative=[0]
    for i in range(len(self.uniq)):
      self.cumulative.append(self.cumulative[i]+len(self.uniq[i]))
    self.index=[{key:idx for idx,key in enumerate(col.keys())} for col in profile]

## Encoding ##
def encode_int(data,profiledata):
  return [ profiledata.index[col][d] for col,d in enumerate(data) ]
  
def encode_categorical(data,profiledata):
  index=[  ]
  for col,d in enumerate(data):
    try:
      index.append( profiledata.index[col][d]+profiledata.cumulative[col] ) 
    except:
      print("I do not know how to encode symbol {} in column {}. This position will remain empty.".format(d,col),file=sys.stderr)
  ret=np.zeros((profiledata.cumulative[-1],),dtype=bool)
  for idx in index:
    ret[idx]=True
  return ret
