#!/usr/bin/env bats

@test "Check build profile" {
  result="$(lbsnn-profile data/seq10.label > output/seq10.profile)"
}

@test "Check encoding" {
  run lbsnn-encode data/seq10.label output/seq10.profile output/seq10.npy
  [ "$status" -eq 0 ]
}
